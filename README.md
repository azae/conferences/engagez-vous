
# Support LeanKanban France 2016

- [Slides](https://gitlab.com/azae/conferences/engagez-vous/builds/artifacts/20161129-lkfr/file/slides.pdf?job=compile_pdf)
- [Notes](https://gitlab.com/azae/conferences/engagez-vous/builds/artifacts/20161129-lkfr/file/notes.pdf?job=compile_pdf)

# Support Agile Tour Rennes 2017

- [Slides](https://gitlab.com/azae/conferences/engagez-vous/builds/artifacts/master/file/slides.pdf?job=compile_pdf)
- [Notes](https://gitlab.com/azae/conferences/engagez-vous/builds/artifacts/master/file/notes.pdf?job=compile_pdf)

